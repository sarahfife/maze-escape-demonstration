﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System;

namespace Maze_Escape
{
    class Spike : Sprite
    {
        // ------------------
        // Behaviour
        // ------------------
        public Spike(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
