﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Maze_Escape
{
    class Player : Actor // Player inherits from ("is an") Actor
    {
        // Data
        private int score = 0;
        private Level levelObject;
        private int lives = 3;


        // Constants
        private const float MOVE_ACCEL = 100000;
        private const float MOVE_DRAG_FACTOR = 0.4f;
        private const float MAX_MOVE_SPEED = 500;
        private const float MIN_ANIM_SPEED = 10;

        // ------------------
        // Behaviour
        // ------------------
        public Player(Texture2D newTexture, int newFrameWidth, int newFrameHeight, float newFramesPerSecond, Level newLevelObject)
            : base(newTexture, newFrameWidth, newFrameHeight, newFramesPerSecond)
        {
            // STUFF
            AddAnimation("walkDown", 0, 3);
            AddAnimation("walkRight", 4, 7);
            AddAnimation("walkUp", 8, 11);
            AddAnimation("walkLeft", 12, 15);

            PlayAnimation("walkDown");

            levelObject = newLevelObject;
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // Check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;

            if(keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
                PlayAnimation("walkLeft");
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
                PlayAnimation("walkRight");
            }

            if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
                PlayAnimation("walkUp");
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
                PlayAnimation("walkDown");
            }

            // Add the movement change to the velocity
            velocity += movementInput * MOVE_ACCEL * frameTime;

            // Apply drag from the ground
            velocity *= MOVE_DRAG_FACTOR;

            // If the speed is too high, clamp it to a reasonable max
            if (velocity.Length() > MAX_MOVE_SPEED)
            {
                velocity.Normalize();
                velocity *= MAX_MOVE_SPEED;
            }

            // If the player isn't moving, stop the animation
            if (velocity.Length() < MIN_ANIM_SPEED)
            {
                StopAnimation();
            }

            base.Update(gameTime);
        }
        // ------------------
        public void HandleCollision(Wall hitWall)
        {
            // Determine collision depth (with direction) and magnitude
            // This tells us which direction to move to exit the collision
            Rectangle playerBounds = GetBounds();
            Vector2 depth = hitWall.GetCollisionDepth(playerBounds);

            // If the depth is non-zero, it means we ARE colliding with this wall
            if (depth != Vector2.Zero)
            {
                float absDepthX = Math.Abs(depth.X);
                float absDepthY = Math.Abs(depth.Y);

                // Resolve the collision along the shallow axis, as that is the
                // one we're closer to the edge on and therefore easier to "squeeze out"
                if (absDepthY < absDepthX)
                {
                    // Resolve the collision on the Y axis
                    position.Y = position.Y + depth.Y;
                }
                else
                {
                    // Resolve the collision on the X axis
                    position.X = position.X + depth.X;
                }
            }
        }
        // ------------------
        public void HandleCollision(Coin hitCoin)
        {
            // Collect coin
            // Hide the coin
            hitCoin.SetVisible(false);

            // Add to score
            score += hitCoin.GetScoreValue();
        }
        // ------------------
        public void HandleCollision(Spike hitSpike)
        {
            // Kill the player
            // TEMP
            Kill();
        }
        // ------------------
        public int GetScore()
        {
            return score;
        }
        // ------------------
        public int GetLives()
        {
            return lives;
        }
        // ------------------
        private void Kill()
        {
            --lives; // lose a life
            score = 0;

            levelObject.ResetLevel();
        }
        // ------------------
    }
}
