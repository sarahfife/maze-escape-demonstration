﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System;
using System.IO;

namespace Maze_Escape
{
    class Level
    {
        // ------------------
        // Data
        // ------------------
        Player player;
        Wall[,] walls;
        List<Coin> coins = new List<Coin>();
        List<Spike> spikes = new List<Spike>();
        int tileWidth;
        int tileHeight;
        int levelWidth;
        int levelHeight;
        Text scoreDisplay;
        int currentLevel;
        bool reloadLevel = false;
        Text livesDiscplay;

        // Assets
        Texture2D playerSprite;
        Texture2D wallSprite;
        Texture2D coinSprite;
        Texture2D spikeSprite;
        SpriteFont UIFont;

        // ------------------
        // Behaviour
        // ------------------
        public void LoadContent(ContentManager content, GraphicsDevice graphics)
        {
            playerSprite = content.Load<Texture2D>("graphics/PlayerAnimation");
            wallSprite = content.Load<Texture2D>("graphics/Wall");
            coinSprite = content.Load<Texture2D>("graphics/Coin");
            spikeSprite = content.Load<Texture2D>("graphics/Spike");
            tileWidth = wallSprite.Width;
            tileHeight = wallSprite.Height;

            // Set up UI
            UIFont = content.Load<SpriteFont>("fonts/mainFont");
            scoreDisplay = new Text(UIFont);
            scoreDisplay.SetPosition(new Vector2(10, 10));

            livesDiscplay = new Text(UIFont);
            livesDiscplay.SetPosition(new Vector2(graphics.Viewport.Bounds.Width - 10, 10));
            livesDiscplay.SetAlignment(Text.Alignment.TOP_RIGHT);


            // Create the player once, we'll move them later
            player = new Player(playerSprite, tileWidth, tileHeight, 6, this);

            // TEMP - this will be moved later
            LoadLevel(1);

        }
        // ------------------
        private void PositionPlayer(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            player.SetPosition(tilePosition);
        }
        // ------------------
        private void CreateWall(int tileX, int tileY)
        {
            Wall wall = new Wall(wallSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            wall.SetPosition(tilePosition);
            walls[tileX, tileY] = wall;
        }
        // ------------------
        private void CreateCoin(int tileX, int tileY)
        {
            Coin newCoin = new Coin(coinSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newCoin.SetPosition(tilePosition);
            coins.Add(newCoin);
        }
        // ------------------
        private void CreateSpike(int tileX, int tileY)
        {
            Spike newTile = new Spike(spikeSprite);
            Vector2 tilePosition = new Vector2(tileX * tileWidth, tileY * tileHeight);
            newTile.SetPosition(tilePosition);
            spikes.Add(newTile);
        }
        // ------------------
        public void Update(GameTime gameTime)
        {
            // Update all actors
            player.Update(gameTime);

            // Collision checking

            // Walls collisions
            List<Wall> collidingWalls = GetTilesInBounds(player.GetBounds());
            foreach(Wall collidingWall in collidingWalls)
            {
                player.HandleCollision(collidingWall);
            }

            // Coin collisions
            foreach(Coin eachCoin in coins)
            {
                if (!reloadLevel && eachCoin.GetVisible() == true && eachCoin.GetBounds().Intersects(player.GetBounds()))
                    player.HandleCollision(eachCoin);
            }

            // Spike collisions
            foreach (Spike eachSpike in spikes)
            {
                if (!reloadLevel && eachSpike.GetVisible() == true && eachSpike.GetBounds().Intersects(player.GetBounds()))
                    player.HandleCollision(eachSpike);
            }

            // Update score
            scoreDisplay.SetTextString("Score: " + player.GetScore());

            // Update lives
            livesDiscplay.SetTextString("Lives: " + player.GetLives());

            // Check if we need to reload the level
            if (reloadLevel == true)
            {
                LoadLevel(currentLevel);
                reloadLevel = false;
            }
        }
        // ------------------
        public List<Wall> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Wall> tilesInBounds = new List<Wall>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / tileWidth);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / tileWidth) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / tileHeight);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / tileHeight) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    // Only add the tile if it exists (is not null)
                    // And if it is visible
                    Wall thisTile = GetTile(x, y);

                    if (thisTile != null && thisTile.GetVisible() == true)
                        tilesInBounds.Add(thisTile);
                }
            }

            return tilesInBounds;
        }
        // ------------------
        public Wall GetTile(int x, int y)
        {
            // Check if we are out of bounds
            if (x < 0 || x >= levelWidth || y < 0 || y >= levelHeight)
                return null;

            // Otherwise we are within the bounds
            return walls[x, y];
        }
        // ------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            player.Draw(spriteBatch);

            foreach(Wall eachWall in walls)
            {
                if (eachWall != null)
                    eachWall.Draw(spriteBatch);
            }

            foreach(Coin eachCoin in coins)
            {
                eachCoin.Draw(spriteBatch);
            }

            foreach (Spike eachSpike in spikes)
            {
                eachSpike.Draw(spriteBatch);
            }

            scoreDisplay.Draw(spriteBatch);
            livesDiscplay.Draw(spriteBatch);
        }
        // ------------------
        public void LoadLevel(int levelNum)
        {
            currentLevel = levelNum;
            string baseLevelName = "Levels/level_";
            LoadLevel(baseLevelName + levelNum.ToString() + ".txt");
        }
        // ------------------
        public void LoadLevel(string fileName)
        {
            // Clear any existing level data
            ClearLevel();

            // Create filestream to open the file and get it ready for reading
            Stream fileStream = TitleContainer.OpenStream(fileName);

            // Before we read in the individual tiles in the level, we need to know 
            // how big the level is overall to create the arrays to hold the data
            int lineWidth = 0; // Eventually will be levelWidth
            int numLines = 0;  // Eventually will be levelHeight
            List<string> lines = new List<string>();    // this will contain all the strings of text in the file
            StreamReader reader = new StreamReader(fileStream); // This will let us read each line from the file
            string line = reader.ReadLine(); // Get the first line
            lineWidth = line.Length; // Assume the overall line width is the same as the length of the first line
            while (line != null) // For as long as line exists, do something
            {
                lines.Add(line); // Add the current line to the list
                if (line.Length != lineWidth)
                {
                    // This means our lines are different sizes and that is a big problem
                    throw new Exception("Lines are different widths - error occured on line "+lines.Count);
                }

                // Read the next line to get ready for the next step in the loop
                line = reader.ReadLine();
            }

            // We have read in all the lines of the file into our lines list
            // We can now know how many lines there were
            numLines = lines.Count;

            // Now we can set up our tile array
            levelWidth = lineWidth;
            levelHeight = numLines;
            walls = new Wall[levelWidth, levelHeight];

            // Loop over every tile position and check the letter
            // there and load a tile based on  that letter
            for (int y = 0; y < levelHeight; ++y)
            {
                for (int x = 0; x < levelWidth; ++x)
                {
                    // Load each tile
                    char tileType = lines[y][x];
                    // Load the tile
                    LoadTile(tileType, x, y);
                }
            }

            // Verify that the level is actually playable
            if (player == null)
            {
                throw new NotSupportedException("A level must have a starting point for the player.");
            }
        }
        // ------------------
        private void LoadTile(char tileType, int tileX, int tileY)
        {
            switch (tileType)
            {
                // Player
                case 'P':
                    PositionPlayer(tileX, tileY);
                    break;

                // Wall
                case 'W':
                    CreateWall(tileX, tileY);
                    break;

                // Coin
                case 'C':
                    CreateCoin(tileX, tileY);
                    break;

                // Spike
                case 'S':
                    CreateSpike(tileX, tileY);
                    break;

                // Blank space
                case '.':
                    break; // Do nothing

                // Any non-handled symbol
                default:
                    throw new NotSupportedException("Level contained unsupported symbol "+tileType+" at line "+tileY+" and character "+tileX);
            }
        }
        // ------------------
        public void ResetLevel()
        {
            // Delay reloading level until after the update loop
            reloadLevel = true;
        }
        // ------------------
        private void ClearLevel()
        {
            spikes.Clear();
            coins.Clear();
        }
        // ------------------

    }
}
